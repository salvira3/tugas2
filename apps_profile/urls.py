from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^edit/$', edit_profile, name ='edit_profile'),
	url(r'^set_user_data/$',set_user_data, name = 'set_user_data')
]
