from django.db import models

# Create your models here.
class dataProfile(models.Model):
	username = models.CharField(max_length=20, primary_key= True)
	npm = models.CharField(max_length=20, unique=True)
	profile_picture = models.URLField(default = "http://www.contribcity.com/images/dummy-profile-pic.png")
	nama = models.CharField(max_length=50, default= "Kosong")
	expertise = models.CharField('Expertise', max_length=50)
	email = models.TextField(default="Kosong")
	linkedIn = models.TextField(default="Kosong")

class Expertise(models.Model):
	BEGINNER = 'Beginner'
	INTERMEDIATE = 'Intermediate'
	ADVANCE = 'Advance'
	EXPERT = 'Expert'

	LEVEL_CHOICE = (
		(BEGINNER,1),
		(INTERMEDIATE,2),
		(ADVANCE,3),
		(EXPERT,4)
	)

	def __str__ (self):
		return self.expertise