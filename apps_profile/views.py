from django.shortcuts import render
from apps_login.models import User
from .models import dataProfile
from apps_status.models import StatusKu

# Create your views here.
response = {}
def index(request):
	if 'user_login' in request.session:
		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		try:
			pengguna = User.objects.get(kode_identitas = kode_identitas)
		except Exception as e:
			pengguna = create_new_user(request)
		response['test'] = 'hai halo'

		profile = dataProfile()
		response['nama'] = profile.nama
		response['keahlian'] = profile.expertise
		response['email'] = profile.email
		response['linkedin'] = profile.linkedIn
		html = 'profile/profile.html'
		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		pengguna = User.objects.get(kode_identitas = kode_identitas)

		statusdata = StatusKu()
		banyakstat = StatusKu.objects.filter(pengguna = pengguna).all().count()
		response['status'] = banyakstat

		if banyakstat > 0:
			response['status_akhir'] = StatusKu.objects.last().description
		else:
			response['status_akhir'] = "Tidak ada status terbaru"

		response['login'] = True
		html = 'profile/profile.html'

		profile = dataProfile()
		response['nama'] = profile.nama
		response['keahlian'] = profile.expertise
		response['email'] = profile.email
		response['linkedin'] = profile.linkedIn
		html = 'profile/profile.html'
		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		pengguna = User.objects.get(kode_identitas = kode_identitas)

		statusdata = StatusKu()
		banyakstat = StatusKu.objects.filter(pengguna = pengguna).all().count()
		response['status'] = banyakstat

		if banyakstat > 0:
			response['status_akhir'] = StatusKu.objects.last().description
		else:
			response['status_akhir'] = "Tidak ada status terbaru"

		return render(request, html, response)
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)

### SESSION : GET and SET
def get_data_session(request):
	if get_data_user(request, 'user_login'):
		response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	elif tipe == "role" and 'role' in request.session:
		data = request.session['role']


	return data

def create_new_user(request):
	nama = get_data_user(request, 'user_login')
	kode_identitas = get_data_user(request, 'kode_identitas')
	role = get_data_user(request, 'role')
	pengguna = User()
	pengguna.kode_identitas = kode_identitas
	pengguna.role = role
	pengguna.nama = nama
	pengguna.save()

	return pengguna

def edit_profile(request):
	html = 'profile/edit.html'
	response['kode_identitas'] = get_data_user(request,'kode_identitas')
	return render(request,html,response)

def set_user_data(request):
	if request.method == 'POST':
		name = request.POST['name']
		email = request.POST['email']

		#bikin model
		# HELP

	return HttpResponseRedirect(reverse('profile:edit_profile'))

