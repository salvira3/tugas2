from django.conf.urls import url
from .views import index, add_status, delete_status

urlpatterns = [
	url(r'^(?P<username>.*)/$', index, name='index'),
	url(r'^add_status', add_status, name='add_status'),
]