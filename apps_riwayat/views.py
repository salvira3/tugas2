from django.shortcuts import render
from django.http import HttpResponseRedirect
from .api_riwayat import get_matkul
# Create your views here.
response = {}
def index(request):
	if get_data_user(request, 'user_login'):
		set_data_for_session(request)
		response['login'] = True
		response['matkuls'] = get_matkul().json()
		html = 'riwayat/riwayat.html'
		return render(request, html, response)
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	elif tipe == "role" and 'role' in request.session:
		data = request.session['role']


	return data
